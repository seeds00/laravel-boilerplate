# Laravel Boilerplate

```
docker-compose up -d --build
```

## Access
`http://localhost:81/`

## Create Laravel Project
`docker-compose exec app composer create-project laravel/laravel src`

### Composer update

`docker-compose exec app composer update`
